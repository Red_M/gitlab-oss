# GitLab Ultimate or Gold for Open Source Projects

We take our responsibility of open source stewardship very seriously (https://about.gitlab.com/2016/01/11/being-a-good-open-source-steward/ and https://about.gitlab.com/stewardship/).

GitLab exists today in large part thanks to the work of hundreds of thousands of open source contributors around the world. To give back to this community who gives us so much, we want to help teams be more efficient, secure, and productive. We believe the best way for them to achieve this is by using as many of the capabilities of GitLab as possible.

It has already been the case for years that that any public project on GitLab.com gets all Gold features. We are happy to now offer a complimentary license to GitLab Ultimate (self-hosted) or subscription to GitLab Gold (SaaS) to all open source projects.

## Here's how to apply

1.   Create a gitlab.com account for your open source project: https://gitlab.com/users/sign_in
1.   Edit this file and add an entry to the [Open source projects using GitLab Ultimate or Gold](#open-source-projects-using-gitlab-ultimate-or-gold)
section at the bottom of this page (all lines required):

     ```
     ### Project name
     A short description of what you do and why.
     https://myawesomeproject.org
     Ultimate or Gold?
     ```

1.   Commit your changes to a new fork and start a new Merge Request.
1.   Fill out the form at https://about.gitlab.com/open-source/

## Requirements

To apply:
- You need to be a project lead or a core contributor for an active open source project.
- Your project needs to use an [OSI-accepted open source license](https://opensource.org/licenses/alphabetical#)
- Your project must not seek to make profit from the resulting project software.

If you or your company work on commercial projects, consider our [plans for businesses](https://about.gitlab.com/pricing/).
If you're not sure if your project meets these requirements, please [contact our support team](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=334447) for help.

We'll review all requests and accept them at our discretion. If accepted, your project will be listed below and we will contact you.

## License/subscription details

- You'll receive a 1 year license for GitLab Ultimate or subscription for GitLab Gold.
- Support is not included, but can be purchased for 95% off, at $4.95/user/month. [Contact Sales](https://ultimate-free-post.about.gitlab.com/sales/) for that.
- Your license/subscription can be renewed each year if your project still meets the requirements.
   - [Contact Sales](https://ultimate-free-post.about.gitlab.com/sales/) 30 days before your license/subscription ends.
- Licenses and subscriptions cannot be transferred or sold.

## Open source projects using GitLab Ultimate or Gold

    ### GNU Mailman
    GNU Mailman is a free and open source mailing list manager. We use Gitlab for all our [development](https://gitlab.com/mailman) and Continuous Integration.
    https://list.org
    Gold

    ### Manjaro Linux
    Manjaro is a user-friendly Linux distribution based on the independently developed Arch operating system. We use Gitlab for all our [development](https://gitlab.manjaro.org) and Continuous Integration.
    https://manjaro.org
    Ultimate

    ### NOC
    NOC is web-scale Operation Support System (OSS) for telecom and service providers. Areas covered by NOC include Network Resource Inventory (NRI), IP Address Management (IPAM), Fault Management (FM), Performance Management (PM), Peering Managemen. System is developed by telecom professionals and for telecom professionals.
    https://nocproject.org/
    Ultimate

    ### eelo
    An open source mobile phone OS that respects user’s data privacy
    https://www.eelo.io/
    Ultimate

    ### Mastodon
    An open source decentralized social network based on open web protocols.
    https://joinmastodon.org
    Ultimate

    ### Ninja Forms
    An open source drag-and-drop form builder for WordPress
    https://ninjaforms.com/
    Ultimate

    ### CHVote
    CHVote is one of only two accredited electronic voting systems by the Federal Council in Switzerland.
    https://republique-et-canton-de-geneve.github.io/chvote-1-0
    Ultimate

    ### Aurora OSS
    An open source ecosystem to provide an alternate to Google Ecosystem. Currently we provide AuroraStore as an alternate to Google PlayStore (https://gitlab.com/AuroraOSS/AuroraStore)
    https://gitlab.com/AuroraOSS/
    Ultimate

    ### Chakra Linux
    A community-developed GNU/Linux distribution with an emphasis on KDE and Qt technologies, utilizing a unique half-rolling release model that allows users to enjoy the latest versions of the Plasma desktop and their favorite applications on top of a periodically updated system core.
    https://www.chakralinux.org
    Ultimate

    ### CiviCRM
    CiviCRM is a web-based Open Source contact relationship management (CRM) system. CiviCRM emphasizes communicating with individuals, community engagement, activism, outreach, managing contributions, and managing memberships.
    https://civicrm.org/
    Ultimate

    ### SECU
    SЁCU is a service that allows you to send password protected self-destructing data packages. Thus the recipient will have to provide a password in order to open a package. And once it is opened, it will no longer be available.
    https://secu.su/
    Ultimate

    ### Spack
    Spack is a package manager for supercomputers, used by HPC centers and developers worldwide.
    https://gitlab.com/spack/spack
    Ultimate

    ### Alchemy Viewer
    A client for SecondLife/OpenMetaverse protocol compatible virtual world platforms.
    https://www.alchemyviewer.org
    Ultimate

    ### dps8m
    The dps8m project is an open source collaboration to create an emulator for the Honeywell DPS8/M mainframe computer, with the goal of running th Multics operating system.
    Ultimate

    ### mvdsv
    MVDSV: a QuakeWorld server
    https://github.com/deurk/mvdsv
    Ultimate

    ### Personal Management System
    Personal Management System is a system used to showcase not only your own personal projects and accomplishments, but also serve as your resume and blog engine. A one stop shop for all your personal branding needs.
    https://repo.theoremforge.com/me/PMS
    Ultimate

    ### OpenAPI
    Rest API for Puppet & Ansible
    https://cyberox.org/rails/openapi
    Ultimate

    ### Ruetomel
    An educative project for CS students to learn about devops, based on a given stack (java, springboot, docker, git/gitlab, k8s)
    https://www.devops.pf or https://gitlab.com/teriiehina/ruetomel
    Ultimate

    ### Groovybot
    Groovy is a feature-rich Discord Bot. His main-feature is to play specific songs available on YouTube via high quality streaming.
    https://groovybot.xyz
    Ultimate

    ### owlo
    An open source ActivityPub utilizing microblogging and fiction platform.
    http://localtoast.net/
    Ultimate

    ### MathLibrary
    A Kotlin library to assist you in Multivariable Calculus, Linear Algebra, Electrostatics, and Quantum Computing.
    https://github.com/ethertyper/mathlibrary
    Ultimate

    ### Ansible - PostgresXL cluster
    The main goal of this project is to have an ansible installer for full Postgres-XL cluster with gtms, coordinators, masters and slaves. The other goal is to have tests using role provision docker, to check behavior in a real environment.
    https://gitlab.com/elrender/postgres-xl-cluster
    Ultimate

    ### CoCoMS - Construction Correspondence Management System
    CoCoMS is a simple Document Management System designed specifically for the management of correspondence generated during the execution of a construction project. CoCoMS is targeted at document controllers and key staff of a construction project.
    https://gitlab.com/chrmina/cocoms
    Ultimate

    ### lainradio.club
    Lainradio.club is an open-source static website using Hugo as the generator. It's purpose is to simply aggregate past [radio] events (usually bi-friday evenings) and other useful stuff.
    https://lainradio.club
    Ultimate

    ### OpenGAG
    An alternative to 9gag. But 100% open source. Driven by the community a bit like this [awesome thing](https://gitlab.com/gitlab-org/gitlab-ce)
    No url yet;
    Ultimate

    ### Minecordbot
    A powerful way to bridge Minecraft and Discord.
    https://minecordbot.cyr1en.com
    Ultimate

    ### Atomix
    A reactive Java framework for building fault-tolerant distributed systems.
    http://atomix.io
    Gold

    ### Open Motors
    Develop a modular open source electric car platform (Hardware & Software) that enables businesses and startups to design, prototype, and build electric vehicles and transportation services.
    https://openmotors.co
    Ultimate

    ### Gestures
    A minimal Gtk+ GUI app for libinput-gestures (Linux touchpad gestures)
    https://gitlab.com/cunidev/gestures
    Ultimate

    ### CacheRefs
    Open source module for Drupal 8 that provides advanced caching invalidations. We are also currently in the process of migrating our other opensource projects to this gitlab instance which include other drupal modules, docker images, alpine linux support for wkhtmltopdf with QT support
    https://git.alloylab.com/open-source/cacherefs
    Ultimate

    ### Nektar++
    Nektar++ is a cross-platform open-source framework for the spectral/hp element method. It is designed to support the construction of efficient, high-performance scalable solvers for a wide range of partial differential equations (PDE) encompassing a number of scientific fields.
    https://www.nektar.info/
    Ultimate

    ### Identihub
    Open Source Design software to host visual assets easily on a page and make it easier to share them in any format. AGPL Free Software.
    https://identihub.co
    Ultimate

    ### firma-cda
    Clinical Documents Repository and Digital Signature using HL7-CDA and IHE-DSG standard for Ecuador Ministry of Health and any other Hospital or Health institution
    https://gitlab.com/MSP_EC/firma-cda
    Ultimate

    ### Human Cell Atlas
    To create comprehensive reference maps of all human cells—the fundamental units of life—as a basis for both understanding human health and diagnosing, monitoring, and treating disease.
    https://www.humancellatlas.org
    Ultimate

    ### BillRun Project
    An open source billing for big-data
    https://git.bill.run/
    Ultimate

    ### equalOS
    Currently an open source site and CI pipeline using Hugo and PostCSS. Can be cloned and served locally. Project is for creating an automated CI pipeline for building and distributing Linux from source inside containers using a tiling window manager, like i3 or Sway.
    https://equalos.org
    Gold

    ### pgjdbc
    JDBC driver for PostgreSQL
    jdbc.postgresql.org
    Gold

    ### CavApps
    A standalone 'home' application that any Gamming clan can use to manage their community, along with plugin tooling and creation.
    https://7cav.us
    Gold

    ### Arctic Engine
    Arctic Engine is an open-source free game engine released under the MIT license. Arctic Engine is implemented in C++ and focuses on simplicity. Many developers have forgotten exactly why it is that we make games. It's joyless, disillusioning and discouraging for them. In the 80's and 90's it was possible for a programmer to make a game alone and it was Fun. Arctic Engine returns the power to the C++ programmer and makes game development fun again.
    https://gitlab.com/huldra/arctic
    Gold

    ### Validity
    A browser extension for validating HTML.
    https://www.validity.org.uk/
    Gold

    ### Wownero
    Wownero is a fairly launched software fork of the privacy focused cryptocurrency Monero without a pre-mine. The project aims to implement experimental blockchain features while having fun as a meme coin.
    http://wownero.org
    Gold

    ### coala
    Linting and fixing code for all languages.
    https://coala.io
    Gold

    ### Better With Mods
    A highly modular hardcore mod for Minecraft.
    https://betterwithmods.com
    Gold

    ### Pterodactyl
    Pterodactyl Panel is the free, open-source, game agnostic, self-hosted control panel for users, networks, and game service providers. Control all of your games from one unified interface.
    https://pterodactyl.io
    Gold

    ### Splits I/O
    A sharing and analyzation tool for speedrunners!
    https://splits.io/
    Gold

    ### Aetherya
    Aetherya is an open-source moderation/utilitarian bot created for a Twitch streamer's Discord server. She tracks users joining, users leaving, users editing/deleting their messages, and has taken a large workload off the mod team.
    https://gitlab.com/TotallyAWeebDev/Aetherya
    Gold

    ### Mastalab
    Mastalab is a multi-accounts Android client for Mastodon
    https://tom79.bitbucket.io
    Gold

    ### Hasadna - The Public Knowledge Workshop
    We release public information and make it easy for the public to meaningfully engage with the data, using open-source and free-software.
    http://www.hasadna.org.il/en/about/
    Gold

    ### Hamakor
    We promote and assist in promoting open-source and free-software in Israel. As part of that, we hold monthly meetups where volunteers contribute to various open-source projects, as well as mentoring new comers to open-source.
    https://www.hamakor.org.il/en/
    Gold

    ### Community Hass.io Add-ons for Home Assistant
    The primary goal of this project is to provide Hass.io / Home Assistant users with additional, high quality, add-ons that allow you to take their automated home to the next level.
    https://github.com/hassio-addons/repository/
    Gold

    ### JRebirth
    JRebirth is a JavaFX Application Framework used to build efficient desktop applications.
    http://www.jrebirth.org
    Gold

    ### Homeless Intake Manager
    Web based software to manage homeless shelter intake and pantry use
    http://www.switchpointcrc.org/switchpoint_home.php
    Gold

    ### Loopring Protocol
    Loopring is a protocol for building decentralized exchanges. Besides the protocol smart-contracts, Loopring also offers a collection of open-sourced software to help you build decentralized exchanges.
    https://loopring.org
    Gold

    ### aGrUM
    aGrUM is a C++ library designed for easily building applications using graphical models such as Bayesian networks, influence diagrams, decision trees or Markov decision processes.
    http://agrum.gitlab.io
    Gold

    ### Cacophony
    Cacophony is an open-source Discord Bot built using microservices for improved reliability and performance.
    https://gitlab.com/Cacophony
    Gold

    ### Python Discord
    We're a large, friendly community focused around the Python programming language, open to those who wish to learn the language or improve their skills, as well as those looking to help others. We're a completely voluntary community of Python lovers, and we're passionate about helping our users learn.
    https://pythondiscord.com
    Gold

    ### NoSQLMap
    Automated NoSQL database enumeration and web application exploitation tool for security professionals.
    http://nosqlmap.net/
    Gold

    ### OmniROM
    OmniROM is a Android custom ROM variant.
    https://www.omnirom.org
    Gold

    ### Freeradius Admin
    This project is a web GUI for a FreeRADIUS 3 server with a MySQL backend.
    https://freeradiusadmin-demo.junelsolis.com/
    Gold

    ### AzuraCast
    A self-hosted, all-in-one, turnkey web radio management suite, including a powerful and intuitive web interface for managing every aspect of a web radio station.
    https://azuracast.com
    Gold

    ### Ownlinux
    A Cross Linux from Scratch based Linux Distribution
    https://gitlab.com/overflyer/ownlinux
    Gold

    ### LeafPic
    An ad-free, open-source and material-designed android gallery alternative
    https://gitlab.com/HoraApps/LeafPic
    Gold

    ### PASSY
    Creating an open source password manager solution.
    https://passy.pw
    Gold

    ### Drupal Test Traits
    Traits for testing Drupal sites that have user content (versus unpopulated sites).
    https://gitlab.com/weitzman/drupal-test-traits
    Gold

    ### Board Summary For Trello Chrome Extension
    The Board Summary for Trello extension for Google Chrome retrieves and displays summary data for Trello boards, and allows for creating nested boards (i.e. cards that reference other boards).
    https://gitlab.com/aarongoldenthal/BoardSummaryForTrelloChromeExtension
    Gold

    ### Robigalia
    A highly reliable, persistent capability OS built in rust on the seL4 microkernel.
    https://robigalia.org
    Gold

    ### emberclear
    This project is for demonstrating the latest features of ember and eventually a learning playground for implementation of a private mesh network over the internet.  The application itself is totally encrypted p2p chat.
    https://gitlab.com/NullVoxPopuli/emberclear
    Gold

    ### OrangeFox Recovery Progect
    Fork of TeamWinRecoveryProject(TWRP) with many additional functions, redesign and more
    https://mryacha.github.io/OrangeFox-Site/
    Gold

    ### Joiner
    Joiner is a Java library which allows to create type-safe JPA queries
    https://gitlab.com/eencircled/Joiner
    Gold

    ### EOS Design System
    The EOS Design System is an open source set of guidelines, elements, components, and layouts made to help developers deliver consistent user experience and interfaces, while they concentrate on what they do best: code.
    http://eos-test.herokuapp.com/
    Gold

    ### papirus-netapp
    PaPiRus Netapp is a free script for Raspberry Pi's that uses the PaPiRus E-Paper display to carry out various network testing scripts.
    https://www.talktech.info
    Gold

    ### Tinity
    A open source MMO framework for developing out the backend of online connected games.
    https://www.trinitycore.org/
    Gold

    ### Recultis
    Return to the cult games. On Linux, with modern, open source engines.
    https://makson.gitlab.io/Recultis/
    Gold

    ### CleverSheep
    An open source high level asynchronous testing framework
    Gold

    ### GLPI
    GLPI stands for Gestionnaire Libre de Parc Informatique is a Free Asset and IT Management Software package, that provides ITIL Service Desk features, licenses tracking and software auditing.
    http://glpi-project.org/
    Gold

    ### Coinbot
    Coinbot is a discord bot to check the price of many cryptocurrencies.
    https://coinbot.ovh
    Gold

    ### VerusCoin
    VerusCoin is a new, mineable and stakeable cryptocurrency. It is a live fork of Komodo that retains its Zcash lineage and improves it.
    https://veruscoin.io/
    Gold

    ### Flowee
    Flowee is a Bitcoin Cash full node implementation that is made into a powerful hub. Flowee the Hub provides services to any developer that wants to connect to the Bitcoin network. Our slogan is "The shortest path to Bitcoin".
    http://flowee.org
    Gold

    ### LeoFS
    An Enterprise Open Source Storage. It is a highly available, distributed, eventually consistent object store.
    https://leo-project.net/leofs/
    Gold

    ### LogicForall: Categorical Syllogisms
    Generates exercises, syllogisms, and propositions of categorical logic. The API can serve this and other educational resources. The UI emphasizes students and instructors to create a positive and accessible learning experience.
    http://logicforall.org/
    Gold

    ### PisiLinux
    Pisi Linux is an open source Linux operating system built around the KDE desktop environment and based on the formaly Pardus Linux distribution.
    https://www.pisilinux.org
    Gold

    ### TatSu
    竜 TatSu generates Python parsers from grammars in a variation of EBNF.
    https://gitlab.com/neogeny/TatSu
    Gold

    ### LibreHealth
    LibreHealth is a collaborative community for free & open source software projects in Health IT, and is a member project of Software Freedom Conservancy.
    https://librehealth.io
    Gold

    ### infoDisplay
    infoDisplay is a telegram bot whose purpose is to have a screen which displays information (pictures or videos) at, for example, schools. I decided to develop it, because everyone in charge of it should be able to upload things to it from home too with a nice looking GUI and without the need to set up a server on ones own.
    https://gitlab.com/liketechnik/infoDisplay
    Gold

    ### Aurora Framework / Aurora Free Software
    A Powerful General Purpose Framework / Free Software Collection
    https://aurorafw.lsferreira.net/
    Gold

    ### OpenSCAD
    OpenSCAD is a multi-platform solid 3D modeling tool (GPL).
    http://openscad.org
    Gold

    ### Fairytale
    Community centric file archiver with state-of-the-art features (recompression, deduplication), giving the users all options from best speed to best compression.
    https://github.com/schnaader/fairytale
    Gold

    ### Zclassic Community Edition
    An open source decentralized p2p permissionless public blockchain leveraging zk-SNARK technology through ZCL cryptocurrency to promote privacy & financial freedom.
    https://zclassic-ce.org
    Gold

    ### Augur
    Augur is an open-source, decentralized, peer-to-peer oracle and prediction market platform built on the Ethereum blockchain.
    https://www.augur.net/

    ### HolyDragon Project
    An open source android based fork of OmniROM for OnePlus and other devices
    https://gitlab.com/HolyDragonProject/android
    Gold

    ### Hyper-Expanse
    Project scaffolding and release tools to streamline the release process through automation.
    https://gitlab.com/hyper-expanse/open-source
    Gold

    ### Open Cloud Platform
    An open source cloud platform to automate & orchrestrate hybrid cloud and containers. providing IaaS for container for open source & entreprenures.
    https://stackhatch.org
    Gold

    ### GieselaDev
    We're passionate about open-source projects, and love developing intuitive, clean applications ranging from audio to user management - mainly for the Discord platform.
    http://gitlab.giesela.io
    Ultimate

    ### Free Software Mirror Group
    The Free Software Mirror Group is a New Zealand based organisation that provides local mirroring of free software. Our goal is to provide official, high quality, outage free mirroring to New Zealand and the Pacific Islands.
    https://fsmg.org.nz/
    Gold

    ### Orbital Bus
    Orbital Bus is an enterprise service bus project that is mesh-based rather than hub-and-spoke. It seeks to enable enterprises to explore the possibilities of ESB without worrying about licensing costs and with a language-agnostic approach.
    http://orbitalbus.com/
    Gold

    ### Orbital CodeGeneration
    The Orbital Code Generation project is a companion for the Orbital Bus project. It is used to generate components that work with the receiver for consumer services.
    http://codegeneration.orbitalbus.com/
    Gold

    ### Orbital Adapters
    One of the essential components for the Orbital Bus are Adapters. The adapters allow the receiver to communicate to consumers over different protocols, databases, or file systems. Over time more Orbital Adapters will appear for more protocols, databases, and file systems.
    http://adapters.orbitalbus.com/
    Gold

    ### Orbital Connectors
    Orbital Connector is a term we use to refer to the integration library used by producers to send messages to the Receiver. The Connector also handles any response, fault, or timeout received in return. Over time more Orbital Connectors will appear for more languages.
    http://connectors.orbitalbus.com/
    Gold

    ### Sandwich Cloud
    An open source VMWare orchestration system that adds public cloud like features to traditional VMWare vCenter deployments.
    https://github.com/sandwichcloud
    Gold

    ### Budgt
    Helping you plan and keep track of your budget.
    https://gitlab.pahofmann.com/pahofmann/budget
    Ultimate

    ### Jenkins GitLab plugin
    The de-facto Jenkins plugin providing integration with GitLab.
    https://github.com/jenkinsci/gitlab-plugin
    Ultimate

    ### Glucosio
    Glucosio is an open source project dedicated to bringing open source apps to smartphone, desktop and web in order to help people with diabetes improve their health outcomes by better self-management of their disease.
    https://www.glucosio.org
    Ultimate

    ### UBports
    Developer and Mantainer of mobile operative system Ubuntu touch and the Unity8 desktop environment stack
    http://ubports.com/
    Gold

    ### Parity
    Blockchain clients (Bitcoin, Ethereum, Polkadot), written in Rust
    https://github.com/paritytech
    Ultimate

    ### USSRM
    Minecraft server software written entirely in Rust, making Minecraft fast and easy to use so that you don't have to. Dubbed the United Soviet Repositories of Minecraft.
    https://gitlab.com/USSRM
    Gold

    ### LAVA Software Project
    LAVA is a continuous integration system for deploying operating systems onto physical and virtual hardware for running tests.
    https://www.linaro.org/initiatives/lava/
    Ultimate

    ### TygerCaddy
    A Python based reverse proxy app with Web GUI
    https://tygercaddy.com
    Ultimate

    ### Hunter 2
    Hunter 2 is a platform for running puzzle hunts
    https://gitlab.com/hunter2.app/hunter2
    Gold

    ### Free Speedway Manager
    Free Speedway Manager, simple speedway sport manager game, python, pyqt.
    https://gitlab.com/freesm/freesm
    Ultimate

    ### Gibberfish
    Gibberfish provides free software tools to activists
    https://gibberfish.org
    Gold

    ### Pleio
    Pleio is an open source collaboration platform for governments.
    https://www.pleio.nl
    Gold

    ### Machinecoin Project
    Machinecoin is a peer-to-peer, decentralized crypto-currency, based on Litecoin which was created back in 2014.
    https://machinecoin.io
    Ultimate

    ### Zamphyr
    Open and free school platform for School 2.0
    https://zamphyr.com
    Gold

    ### TaxonWorks
    TaxonWorks is an open-source workbench for those studying the Earth's Biodiversity.
    http://taxonworks.org
    Ultimate


    ### CallerInfo Project
    An android app to get phone number location and other info.
    https://github.com/xdtianyu/CallerInfo
    Ultimate

    ### Zest Kernel Projecc
    Custom Linux kernels for various Android devices
    https://gitlab.com/ZKP
    Gold

    ### Unnamed Arma Group
    We are an active group of modders for Bohemia Interactive's Arma franchise, all of our work is open-source and available to the community.
    https://gitlab.com/uag
    Gold

    ### WarEmu
    A Warhammer Online Emulator 1.4.8
    https://github.com/WarEmu/WarEmu
    Ultimate

    ### Autosubmit
    Autosubmit is a python-based tool to create, manage and monitor experiments by using Computing Clusters, HPC’s and Supercomputers remotely via ssh.
    https://earth.bsc.es/gitlab/es/autosubmit
    Ultimate    

    ### Project Yaotsu
    Project Yaotsu is an open source framework for computer architecture simulation
    https://gitlab.com/yaotsu

    ### Panto
    A modern monitoring solution
    https://pantomath.io

    ### ZeroGravity CMS
    A flat-file based CMS to be used as a drop-in content solution for Symfony projects.
    https://gitlab.com/zero-gravity/zero-gravity-cms

    ### RVM
    RVM is a command-line tool which allows you to easily install, manage, and work with multiple Ruby versions.
    https://rvm.io
    Gold

    ### Passit
    Passit is an open source, web based password manager that focuses on simplicity and group sharing. Source code is available at http://gitlab.com/passit/
    https://passit.io

    ### Inko
    Inko is a gradually-typed, safe, object-oriented programming language for writing concurrent programs.
    https://inko-lang.org/
    Ultimate

    ### Shizuku
    Shizuku is GPU Pathtracing renderer for hobby artists.
    https://gitlab.com/shizuku-render/Shizuku
    Gold

    ### HydrOffice
    A research framework in ocean mapping (https://www.hydroffice.org)
    https://gitlab.com/HydrOffice
    Gold
    
    ### Fudaa
    Integration Platform For Scientific Codes
    https://fudaa-project.atlassian.net/wiki/spaces/FUDAA/overview
    ### SIM-ScALA-BIM
    SIM-ScALA-BIM is an Abraca-what? board game clone written in Scala as final project elaboration for University exam.
    https://gitlab.com/abra-team/sim-scala-bim
    Gold

    
    ### eQuartier
     eQuartier is an association under the French law of 1901. We aim to give the local independant shops a way to take back the marketshare of big retail stores and online retailers. We empower local communities build around small local bike transportation companies, local shopkeepers and inhabitants. To do so we're developping an AGPL licenced web platform, first with prestashop and next based on solid project together with the cooperativism platform community in France.
     https://equartier.fr
     Gold
    ### malscan
    A linux malware scanner for web servers.
    https://malscan.com
    Gold

    ### Typhon
    Typhon is a secrets manager that aims to provide similar function to an HSM or extend the usage of an HSM.
    https://git.blesstherains.africa/typhon
    Ultimate
    Ultimate

    ### OpenCI
    Our vision is to bring our diverse open source communities together by facilitating the communication between us and our integration, deployment, delivery, and automation.
    https://openci.io
    Gold
    
     ### RiSiRiPlugin
    RiSiRiPlugin is a open-source plugin for schools with an warehouse that want to keep track of there stuff. 
    https://github.com/MextroNL/risiriplugin
    Gold
    ### Fantom Foundation
    Fantom Foundation engages in blockchain research and open-sources everything.
    https://fantom.foundation
    Ultimate
    
    ### Screencam screen recorder
    Android app to record screen using native API and without root
    https://gitlab.com/vijai/screenrecorder
    Gold
    ### Mpm
    A cross-platform Assembler that supports Zilog Z80/Z180/Z380 CPU assembly language. Future works will support Elf format and Arm + Mips CPU's.
    https://gitlab.com/bits4fun/mpm
    Ultimate
    ### Propositum
    Combine open-source, portable tools to facilitate task & information management, automation, data manipulation and analytics.
    https://gitlab.com/xeijin/propositum
    Gold
    ### PushFish
    We aim to make Push notifications accessible for anyone who doesn't want to rely on others. https://gitlab.com/PushFish
    http://push.fish
    Gold
    Ultimate   

    ### KWB-R
    Kompetenzzentrum Wasser Berlin gGmbH (KWB) is an international centre for water research and knowledge transfer. We are developing open-source tools 
    (mainly in R) for performing our research in a more reproducible way.  
    https://github.com/kwb-r
    Gold   
    
    ### RedServ
     A semi-fast python web framework.
     https://gitlab.com/Red_M/redserv
     Ultimate

